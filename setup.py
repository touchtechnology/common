from setuptools import setup, find_packages

setup(
    name='touchtechnology-common',
    version='3.5.5',
    author='Touch Technology Pty Ltd',
    author_email='support@touchtechnology.com.au',
    url='https://bitbucket.org/touchtechnology/common/',
    description='Common components used in all Touch Technology library code.',
    packages=find_packages(include=['touchtechnology*']),
    install_requires=[
        'django-classy-tags~=0.7.2',
        'django-froala-editor~=2.0',
        'django-guardian~=1.4.5,!=1.4.6',
        'django-mptt~=0.8.6',
        'first~=2.0.1',
        'Pillow~=3.0',
        'python-dateutil~=2.5.3',
        'pytz',
    ],
    extras_require={
        "redis": [
            "redis>=2.10.5,<2.11",
            "hiredis>=0.2.0,<0.3",
            "django-redis-cache>=1.6.3,<1.7"
        ],
    },
    tests_require=[
        'pytz==2013.7',
    ],
    include_package_data=True,
    namespace_packages=['touchtechnology'],
    zip_safe=False,
)
