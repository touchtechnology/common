from os.path import dirname, join as basejoin, realpath

from django.core.urlresolvers import reverse_lazy


def join(*args):
    return realpath(basejoin(*args))


PROJECT_DIR = join(dirname(__file__), '..')
DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:',
    }
}

DATABASE_ROUTERS = (
    'touchtechnology.common.routers.AppRouter',
)

TIME_ZONE = 'Australia/Sydney'
LANGUAGE_CODE = 'en-AU'

USE_TZ = True
STATIC_URL = '/static/'

STATIC_ROOT = join(PROJECT_DIR, 'static')
MEDIA_ROOT = join(PROJECT_DIR, 'media')

SECRET_KEY = '2bksb4rhbv7i1$!5xzux0&amp;&amp;sl2@de@k6sxb4(zlj4l)+xds#2i'
SITE_ID = 1

DEFAULT_FILE_STORAGE = 'touchtechnology.common.storage.FileSystemStorage'

ROOT_URLCONF = 'test_app.urls'
WSGI_APPLICATION = 'test_app.wsgi.application'

SESSION_ENGINE = 'django.contrib.sessions.backends.signed_cookies'

INSTALLED_APPS = (
    'mptt',
    'guardian',

    'touchtechnology.common',
    'example_app',

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',  # without this, you can't do authentication
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'guardian.backends.ObjectPermissionBackend',
)

LOGIN_URL = reverse_lazy('accounts:login')

TOUCHTECHNOLOGY_STORAGE_FOLDER = '/tmp'
TOUCHTECHNOLOGY_STORAGE_URL = '/storage/'

ANONYMOUS_USER_NAME = 'anonymous'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'OPTIONS': {
            'debug': DEBUG,
            'context_processors': (
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.contrib.messages.context_processors.messages',
                'touchtechnology.common.context_processors.env',
                'touchtechnology.common.context_processors.query_string',
                'touchtechnology.common.context_processors.site',
                'touchtechnology.common.context_processors.tz',
            ),
        },
    },
]

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'touchtechnology.common.middleware.TimezoneMiddleware',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)-10s %(name)-30s %(asctime)-27s '
                      '%(message)s',
        },
        'simple': {
            'format': '%(levelname)s %(message)s',
        },
    },
    'handlers': {
        'null': {
            'class': 'logging.NullHandler',
        },
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'touchtechnology': {
            'handlers': ['console'],
            'level': 'ERROR',
        },
    },
}
