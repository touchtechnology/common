from django.conf.urls import include, url
from django.contrib.sitemaps.views import sitemap

from touchtechnology.common.sitemaps import NodeSitemap
from touchtechnology.common.sites import AccountsSite

accounts = AccountsSite()

urlpatterns = [
    url(r'^accounts/', include(accounts.urls)),
    url(r'^sitemap\.xml$', sitemap,
        dict(sitemaps=dict(nodes=NodeSitemap)),
        name='sitemap'),
]
