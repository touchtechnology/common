#!/bin/bash -ex

export DJANGO_SETTINGS_MODULE=test_app.settings
export PYTHONPATH=$PYTHONPATH:$(pwd)

pushd touchtechnology/common
django-admin makemessages -i 'test_*.py' -l en-AU -l de -l fr -l ja
django-admin compilemessages
popd
