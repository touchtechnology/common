RELEASES
--------

3.5.5 - Wednesday 12 October, 2016

 * Fix signature of BooleanSelect.render for Django 1.10

3.5.4 - Wednesday 12 October, 2016

 * Exclude django-guardian==1.4.6 -- failures during testing

3.5.3 - Wednesday 31 August, 2016

 * Add Django 1.10 support

3.5.2 - Friday 26 August, 2016

 * Add test cases for generic_delete and generic_permimssions

3.5.1 - Thursday 25 August, 2016

 * Continuous integration improvements

3.5.0 - Friday 19 August, 2016

 * Python 3.5 compatibility
 * Django 1.7 support dropped
 * Django 1.10 readiness

3.4.3 - Tuesday 26 April, 2016

 * Relax version restriction on django-froala-editor

3.4.2 - Thursday 31 March, 2016

 * Fix AttributeError in do_navigation function

3.4.1 - Tuesday 29 March, 2016

 * Update version of Pillow to resolve https://github.com/mariocesar/sorl-thumbnail/issues/429

3.4.0 - Friday 4 March, 2016

 * Closed #37: added unit tests for generic_edit_multiple

3.3.11 - Monday 25th January, 2015

 * Add tests for storage backends

3.3.10 - Wednesday 30th December, 2015

 * Add django-froala-editor for much improved WYSIWYG editing

3.3.9 - Tuesday 22nd December, 2015

 * Resolve TypeError caused by filtering against ContentType.name field in NodeSitemap

3.3.8 - Saturday 12th December, 2015

 * Remove deprecated Django features - making it possible to use with Django 1.8

3.3.7 - Thursday 10th December, 2015

 * Fixed generic_permissions view to correctly pass all customising parameters

3.3.6 - Wednesday 9th December, 2015

 * Improved control over the generic permission view

3.3.5 - Monday 30th November, 2015

 * Packaging updates only

3.3.4 - Sunday 29th November, 2015

 * Packaging updates only

3.3.3 - Friday 27th November, 2015

 * Improved the show_toolbar_callback - removed dependency on custom permission "debug_toolbar.show"
 * ModelChoiceField and ModelMultipleChoiceField now return a .select_related() queryset by default
 * Permission checking cache implemented in the hasperm template filter for performance gains
 * Update the AppRouter for Django 1.8 compatibility

3.3.2 - Wednesday 4th November, 2015

 * Add custom EmailField which normalises email address to lowercase and length of 254 characters

3.3.1 - Tuesday 13th October, 2015

 * Update to fix modal delete template.

3.3 - Wednesday 16th September, 2015

 * Updates to support MVP admin theme.

3.2.1 - Wednesday 17th June, 2015

 * Fixed #36: when creating a ModelForm dynamically in Application.generic_edit, introspect the field list

3.2 - Friday 5th June, 2015

 *** Targets Django 1.7 & 1.8 ***

 * MVP Ready: updated generic methods for next version of admin application
 * Fixed #34: error when SelectTimeField select lists are null

3.1.1 - Thursday 23rd April, 2015

 * HTTP caching improvements
 * django-tenant-schemas: removed dependency on django.contrib.sites framework

 * Fixed #32: location fields using Google Maps v3 API now load again
 * Fixed #33: disable pagination for generic_list if paginate_by=0

3.1 - Thursday 26th February, 2015

 * django-guardian: add object level permissions to majority of the generic views

 * Fixed #29: add UTC and GMT to the list of TIMEZONE_CHOICES for our custom DateTimeField
 * Fixed #30: ensure timezone activation side-effects are cleared on new requests

3.0.7 - Tuesday 20th January, 2015

 * Fixed continuous testing with new version of drone.io which uses $$ instead of {{ }} for variable substitution.

3.0.6 - Wednesday 14th January, 2015

 * Fixed #27: fix issues when `django.contrib.auth.models.User` is swapped out.

3.0.5 - Wednesday 14th January, 2015

 * Fixed #26: password reset URL pattern matches what is expected by `django.contrib.auth`.

3.0.4 - Friday 14th November, 2014

 * French translation. With thanks to Erick Acker from French Touch Association.

3.0.3 - Thursday 13th November, 2014

 * Fixed #24: pickling of AccountSite is now possible allowing cache to be re-enabled.

3.0.2 - Monday 13th October, 2014

 * Fixed exception handling when attempting to import invalid path construction.

3.0.1 - Sunday 12th October, 2014

 * Fixed a missed use of HttpResponse with deprecated `mimetype` keyword argument.

3.0 - Friday 3rd October, 2014

 *** Targets Django 1.7 ***

 * enhancement-22: improve the show_toolbar_callback logic

2.4.2 - Friday 18th July, 2014

 * Fixed #20: fixed transaction handling in generic_edit_related
 * Added test cases for env, site, tz in context_processors.py

2.4.1 - Tuesday 15th July, 2014

 * Fixed #19: custom SelectDateField and SelectDateTimeField with empty values
 * Final non-core release of South (1.0) added to requirements

2.4 - Saturday 21st June, 2014

 * Fixed #17: add mechanism to have the project self-report on library code updates
 * Fixed #18: allow the pagination template tag to correctly rewrite the query string

2.3.3 - Tuesday 13th May, 2014

 * Fixed #11: resolve issue with email address not appearing in password reset emails
 * Fixed #14: resolve output from twittify template filter
 * Fixed #15: resolve SelectDateTimeField not permitting blank timezones

2.3.2 - Friday 18th April, 2014

 * Fixed #13: Exception from deserialization of blank/null DateTimeField [0674f60]

2.3.1 - Thursday 17th April, 2014

 * Fixed #12: AppRouter no longer causes an ImportError [9ca4edb]
 * Minor update to setup.py packaging

2.3 - Tuesday 8th April, 2014

 * Major cleanup of dependencies, PEP8, and packaging refactor
 * Removed `SettingsAdminComponent` and `UsersGroupsAdminComponent` [b46b4ca]
 * Removed FauxNode implementation [ff7ff94]

2.2.3 - Thursday 22nd August, 2013

 * Handle ImportError and ValueError to disable uninstalled applications [ff1895a]

2.2.2 - Friday 16th August, 2013

 * Revise {% field %} template tag to shift majority of logic to template [34e110f]
 * Fix run-away Application instance memory leak [cfee783]
 * Minor debugging related changes [7f00fa8, c534c06]

2.2.1 - Tuesday 13th August, 2013

 * Fix template escaping in {% field %} template tag in Django 1.5+ [6e03942]

2.2 - Tuesday 13th August, 2013

 * Framework updates - compatability with Django 1.4-1.6 [75a82fe, 0594645]
 * Time zone awareness - with test cases [0341535]
 * Multiple language support [2da2771]
 * Removed admin templates, they belong in touchtechnology-admin package [0031aa5]
 * Various minor improvements [c4590a9, e408d7d, 7f40e68]

2.1 - Sunday 7th July, 2013

 * Added {% host %} and {% hostname %} template tags [d62271f]
 * Added NodeRelationMixin to simplify and improve the {% navigation %} template tag [b6646ec]
 * Abstracted {% version %} template tag to work interchangably with hg & git projects [d27e477]
